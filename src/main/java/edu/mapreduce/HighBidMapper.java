package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.counters.AnyErrorCounter;
import edu.mapreduce.counters.BidNoFormatted;
import edu.mapreduce.dto.BidDto;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class HighBidMapper extends Mapper<Object, Text, Text, Text> {

    private String DELIMETER = "\\s+"; // \s \t character

    private int PRICE_OFFSET_END = 4;

    private int CITY_ID_OFFSET_END = 16;

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        BidDto parsingResult;
        try {
            parsingResult = parseInputString(line);
        } catch (NumberFormatException ex) {
            context.getCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR).increment(1);
            return;
        } catch (Exception ex) {
            context.getCounter(AnyErrorCounter.ANY_ERROR).increment(1);
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(parsingResult);
        context.write(new Text(parsingResult.getCityId()), new Text(json));
    }

    private BidDto parseInputString(String line) {
        String[] parsedBySpace = line.split(DELIMETER);
        return new BidDto(
                parsedBySpace[(parsedBySpace.length - 1) - CITY_ID_OFFSET_END],
                Integer.parseInt(parsedBySpace[(parsedBySpace.length - 1) - PRICE_OFFSET_END]),
                1
        );
    }
}

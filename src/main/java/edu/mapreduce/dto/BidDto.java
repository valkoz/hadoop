package edu.mapreduce.dto;

public class BidDto {

    private Integer bidPrice;
    private Integer countOfBidPrice;
    private String cityId;

    public BidDto() {
    }

    public BidDto(
            String cityId,
            Integer bidPrice,
            Integer countOfBidPrice
            ) {
        this.bidPrice = bidPrice;
        this.countOfBidPrice = countOfBidPrice;
        this.cityId = cityId;
    }

    public Integer getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(Integer bidPrice) {
        this.bidPrice = bidPrice;
    }

    public Integer getCountOfBidPrice() {
        return countOfBidPrice;
    }

    public void setCountOfBidPrice(Integer countOfBidPrice) {
        this.countOfBidPrice = countOfBidPrice;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}

